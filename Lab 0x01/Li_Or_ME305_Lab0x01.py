""" @file                       Li_Or_ME305_Lab0x01.py
    @brief                      Script allowing the user to cycle through three light patterns through button press
    @details                    This script allows the user to change the green LED light pattern
                                on the Nucleo board. The three light patterns in order are: square wave, sine wave,
                                and sawtooth wave. The program first instructs the user on the console to press the
                                blue button, B1, on the Nucleo board which initiates the light cycles. At the beginning
                                of each light pattern, the console will inform the user which pattern is selected.
    @author                     Damond Li
    @author                     Chris Or
    @date
"""

import pyb
import math
import utime

def onButtonPressFCN(IRQ_src):
    """ @brief                  Interrupt callback for when the button is pressed
        @details                This callback has a ButtonIsPressed boolean that returns
                                true when the button is pressed
        @param ButtonIsPressed  Boolean describing the status of the button
    """

    ##  @brief                  Boolean status describing the status of the button (T/F)
    #   @details                Variable describing the status of the button. When the button
    #                           is pressed, this variable becomes true and vice versa.
    #
    global ButtonIsPressed
    ButtonIsPressed = True

# Run continuously until the user ends it.
if __name__ == '__main__':

    # Establish the status of the button
    ButtonIsPressed = False

    ##  @brief              Created object for PC13
    #   @details            Establish the object for button B1 on the Nucleo board
    #
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)

    ##  @brief              Establish the interrupt callback for when the button is pressed
    #   @details            Associates the callback function with the pin by setting up an external interrupt
    #
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=
                           onButtonPressFCN)

    ##  @brief              Created object for the LED (pinA5)
    #   @details            Created object for the LED (pinA5). This will be referenced for adjusting the brightness
    #                       of the LED
    #
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)

    ##  @brief              Created object for the frequency at which the LED updates
    #   @details            Establish the frequency at which the LED updates
    #
    tim2 = pyb.Timer(2, freq=20000)

    ##  @brief              Created object for controlling the pulse-width percentage
    #   @details            This object will be used to control the pulse-width percentage at which the LEF flashes.
    #                       Doing so will adjust our perceived brightness of the LED.
    #
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

    ##  @brief              Initial state of the finite state machine
    #   @details            This state variable will determine which part of the finite-state machine will be run.
    #
    state = 0

    ##  @brief              Variable representing the starting reference time at the beginning of each state
    #   @details            Establishes the beginning time of a new state by restarting the time at the initial point.
    #                       The units of this variable are ms.
    #
    startTime = 0.00

    ##  @brief              Variable representing the current time
    #   @details            This variable sets the current time that will be used to relate to the starting time to
    #                       define the time elapsed. The units of this variable are ms
    #
    currentTime = 0.00

    ##  @brief              Variable representing the elapsed time between the current time and the starting time
    #   @details            The difference between the current and start times that gives each state its own "current
    #                       time". The units of this variable are ms.
    #
    relativeTime = 0.00

    while (True):
        try:
            if (state == 0):
                #   Run State 0 Code
                print('Welcome! Pressing the blue button, B1, on the Nucleo will'
                      ' cycle through three LED patterns: Square, '
                      'Sine Wave, and Sawtooth. Press the button B1 '
                      'to begin!')

                #   Transition to State 1
                state = 1

            elif (state == 1):
                #   Run State 1 Code
                if (ButtonIsPressed == True):
                    #   Advance to next state
                    state = 2
                    #   Reset button status
                    ButtonIsPressed = False
                    #   Reset timer
                    startTime = utime.ticks_ms()
                    #   Notify user of next state
                    print('Square Wave Pattern Selected')

            elif (state == 2):
                #   Run State 2 Code for Square Wave
                if (ButtonIsPressed == False):
                    currentTime = utime.ticks_ms()
                    relativeTime = utime.ticks_diff(currentTime, startTime) % 1000
                    t2ch1.pulse_width_percent((relativeTime < 500) * 100)
                else:
                    #   Swtich to the next state
                    state = 3
                    ButtonIsPressed = False
                    print('Sine Wave Pattern Selected')
                    startTime = utime.ticks_ms()

            elif (state == 3):
                #   Run State 3 Code for Sine Wave
                if (ButtonIsPressed == False):
                    currentTime = utime.ticks_ms()
                    relativeTime = (utime.ticks_diff(currentTime, startTime) % 10000) / 1000
                    rad = relativeTime * 0.2 * math.pi
                    t2ch1.pulse_width_percent(50 * math.sin(rad) + 50)
                else:
                    #   Switch to the next state
                    state = 4
                    ButtonIsPressed = False
                    print('Sawtooth Wave Pattern Selected')
                    startTime = utime.ticks_ms()

            elif (state == 4):
                #   Run State 4 Code for Sawtooth Wave
                if (ButtonIsPressed == False):
                    currentTime = utime.ticks_ms()
                    relativeTime = utime.ticks_diff(currentTime, startTime) % 1000
                    t2ch1.pulse_width_percent(relativeTime / 10)
                else:
                    #   Switch to the next state
                    state = 2
                    ButtonIsPressed = False
                    print('Square Wave Pattern Selected')
                    startTime = utime.ticks_ms()

        except KeyboardInterrupt:
            break

    print('Program Terminated')
