""" @file               closedloop.py
    @brief              Controller driver for closed loop control.
    @details            This controller driver is responsible for closed loop
                        proportional control of the motor. This driver accounts
                        for gain value, Kp, and a desired rotational speed, omega_ref.
    @author             Damond Li
    @author             Chris Or
    @date               November 16, 2021
"""

import DRV8847
import task_encoder
import shares

class ClosedLoop:
    """ @brief                  Driver for a negative feedback loop
        @details                This driver performs controls using proportional gain
    """

    def __init__(self):
        """ @brief              Interface with motors to improve step response
            @details            This class will be referenced in the motor task
                                for a step response.
        """
        ## @brief Create object for desired rotational speed
        self.omega_ref = 0
        ## @brief Create object for set proportional gain value
        self.Kp = 0
        
    def update(self, omega):
        ## @brief Establish instantaneous rotational speed of the motor
        self.omega = float(omega)
        ## @brief Define error object as the difference between desired and instantaneous rotational speed
        self.error = float(self.omega_ref) - self.omega
        ## @brief Define output object as the error multiplied by the proportional gain
        self.output = self.error * float(self.Kp)

        # Limit output to be limited to 100%
        if (self.output >= 100):
            self.output = 100
        elif (self.output <= -100):
            self.output = -100
            
        return self.output
        
    def get_Kp(self):
        return self.Kp  
    
    def set_Kp(self, gain):
        # Setting Kp as the user input gain
        self.Kp = gain
        
    def set_speed(self, speed):
        # Setting omega_ref to the user desired speed
        self.omega_ref = speed
