""" @file               task_motor.py
    @brief              Motor task for the motor driver.
    @details            This task is responsible for establishing the frequency
                        at which data is collected from the encoder driver.
    @author             Damond Li
    @author             Chris Or
    @date               November 16, 2021
"""

import DRV8847
import closedloop
import utime

class Task_Motor:
    """ @brief                  Interface with two DR8847 motors
        @details                This class implements states required in the operation of the
                                motor
    """

    def __init__(self, state1, state2, UI_print1, UI_print2, duty_cycle1, duty_cycle2, gain1, gain2, speed1, speed2, desiredspeed1, desiredspeed2):
        """ @brief                  Constructs a motor task
            @details                The motor task is implemented as a finite
                                    state machine.
            @param  state1          A shares. Share object describing the state of
                                    the finite state machine. (motor/encoder 1)
            @param  state2          A shares. Share object describing the state of
                                    the finite state machine. (motor/encoder 2)
            @param  UI_print1       A queue. Queue object containing the data to
                                    be printed over the VCP for encoder/motor 1.
            @param  UI_print2       A queue. Queue object containing the data to
                                    be printed over the VCP for encoder/motor 2.
            @param  duty_cycle1     A queue. Queue object specifying the duty cycle
                                    for motor 1.
            @param  duty_cycle2     A queue. Queue object specifying the duty cycle
                                    for motor 2.
            @param  gain1           A share. Share object specifying the proportional
                                    gain for motor 1.
            @param  gain2           A share. Share object specifying the proportional
                                    gain for motor 2.
            @param  speed1          A share. Share object for the current motor 1 speed.
            @param  speed2          A share. Share object for the current motor 2 speed.
            @param  desiredspeed1   A share. Share object designating the desired speed
                                    for motor 1.
            @param  desiredspeed2   A share. Share object designating the desired speed
                                    for motor 2.
        """
        ## @brief Create motor object
        self.motor_drv = DRV8847.DRV8847()
        ## @brief Brings the DR8847 motor out of sleep mode
        self.motor_drv.enable()
        ## @brief Initiates motor 1
        self.motor_1 = self.motor_drv.motor(1)
        ## @brief Initiates motor 2
        self.motor_2 = self.motor_drv.motor(2)
        ## @brief Initializing duty cycle for motor 1
        self.motor_1.set_duty(0)
        ## @brief Initializing duty cycle for motor 2
        self.motor_2.set_duty(0)
        ## @brief Gain value for motor 1
        self.gain1 = gain1
        ## @brief Gain value for motor 2
        self.gain2 = gain2
        ## @brief Current speed of motor 1
        self.speed1 = speed1
        ## @brief Current speed of motor 2
        self.speed2 = speed2
        ## @brief Desired speed of motor 1
        self.desiredspeed1 = desiredspeed1
        ## @brief Desired speed of motor 2
        self.desiredspeed2 = desiredspeed2
        ## @brief Duty cycle for motor 1
        self.duty_cycle1 = duty_cycle1
        ## @brief Duty cycle for motor 2
        self.duty_cycle2 = duty_cycle2
        ## @brief A shares.Share object for the state of the finite state machine
        self.state1 = state1
        ## @brief A shares.Share object for the state of the finite state machine
        self.state2 = state2
        ## @brief A shares.Queue object containing the data to be printed over the VCP for motor/encoder 1
        self.UI_print1 = UI_print1
        ## @brief A shares.Queue object containing the data to be printed over the VCP for motor/encoder 2
        self.UI_print2 = UI_print2
        ## @brief Create closed loop driver object for motor 1
        self.CL1 = closedloop.ClosedLoop()
        ## @brief Create closed loop driver object for motor 2
        self.CL2 = closedloop.ClosedLoop()
        ## @brief Boolean describing whether the 10-sec step response is started for motor 1
        self.boolean1 = False
        ## @brief Boolean describing whether the 10-sec step response is started for motor 2
        self.boolean2 = False
        ## @brief Object specifying the period
        self.next_time = 0.20
        ## @brief Initiating the time offset as zero
        self.timeoffset = float(0)
        ## @brief Reference object to determine elapsed time
        self.previous_ticks = utime.ticks_us()

    def run(self):
        """ @brief              Runs one iteration of the finite state machine.
            @details            This function calls to the motor driver
        """

        # State 7 Clears motor fault
        if (self.state1.read() == 7 or self.state2.read() == 7):
            self.motor_drv.enable()
            self.motor_1.set_duty(0)
            self.state1.write(0)
            self.motor_2.set_duty(0)
            self.state2.write(0)

        # State 5 sets duty cycle for motor 1
        if (self.state1.read() == 5):
            self.motor_1.set_duty(self.duty_cycle1.read())
            self.state1.write(0)

        # State 5 sets duty cycle for motor 2
        if (self.state2.read() == 5):
            self.motor_2.set_duty(self.duty_cycle2.read())
            self.state2.write(0)
            
        # State 10 initiates step response for motor 1
        if (self.state1.read() == 10):
            #self.step(1)
            self.CL1.Kp = self.gain1.read()
            self.CL1.omega_ref = self.desiredspeed1.read()
            self.step(1)           
        
        # State 10 also initiates step response for motor 2
        if (self.state2.read() == 10):
            #self.step(2)
            self.CL2.Kp = self.gain2.read()
            self.CL2.omega_ref = self.desiredspeed2.read()
            self.step(2)
        
        # State 11 stops step response and corresponding data collection
        if (self.state1.read() == 11 or self.state2.read() == 11):
            self.end_step(1)
            self.end_step(2)
            self.state1.write(0)
            self.state2.write(0)
            
    def start_step(self, state):
        """ @brief              Starts step response
            @details            This function initiates the step response
                                for 10s at intervals specified
        """
        # Depending on the argument, we will determine which objects to manipulate
        if (state == 1):
            self.boolean1 = True
            self.start_ticks1 = utime.ticks_us()
            self.CL1.set_speed(self.desiredspeed1.read())
            self.CL1.set_Kp(self.gain1.read())
            self.step(1)
        elif (state == 2):
            self.boolean2 = True
            self.start_ticks2 = utime.ticks_us()
            self.CL2.set_speed(self.desiredspeed2.read())
            self.CL2.set_Kp(self.gain2.read())
            self.step(2)
        self.next_time = utime.ticks_diff(utime.ticks_us(), self.previous_ticks) / 1_000_000
        self.next_time += 0.20
        
    def step(self, state):
        """ @brief              Period of step response for 10 sec
            @details            This function runs the step response
                                for the required 10 sec by counting the time
                                elapsed since the reference time.
            @param    state     A share. Share object describing which motor/encoder
                                combination is being run.
        """

        # Check if the step response has started
        if (state == 1 and self.boolean1 == False):
            self.start_step(1)
        elif (state == 2 and self.boolean2 == False):
            self.start_step(2)

        # Depending on the argument, set the proper temp variables
        if (state == 1):
            start_ticks = self.start_ticks1
            duty = self.CL1.update(self.speed1.read())
            speed = self.speed1.read()
            self.motor_1.set_duty(duty)
            UI_print = self.UI_print1
        elif (state == 2):
            start_ticks = self.start_ticks2
            duty = self.CL2.update(self.speed2.read())
            speed = self.speed2.read()
            self.motor_2.set_duty(duty)
            UI_print = self.UI_print2
        
        # Function begins
        current_ticks = utime.ticks_us()
        self.current_time = utime.ticks_diff(current_ticks, 
                                             self.previous_ticks) / 1_000_000 
        if (utime.ticks_diff(current_ticks, start_ticks) < 10_000_000):
            if (self.current_time - self.next_time >= 0):
                if (self.timeoffset == 0):
                    self.timeoffset = utime.ticks_diff(current_ticks, start_ticks)
                    
                UI_print.put([(utime.ticks_diff(
                    current_ticks, start_ticks)-self.timeoffset) / 1_000_000, 
                    speed, duty])
                self.next_time += 0.20
        else:
            self.end_step(state)
            
    def end_step(self, state):
        """ @brief                  Ends step response and corresponding data collection
            @details                This function terminates the step response and the collection of data
                                    by switching the boolean variable to false and returning the state to zero.
                """
        # Function to end the step response
        # Also checks if the step response has started or not
        self.timeoffset = 0
        if (state == 1):
            if (self.boolean1 == True):
                self.boolean1 = False
                self.motor_1.set_duty(0)
                self.state1.write(0)
            else:
                # Simply transition to state 0 if data collection did not start yet.
                self.motor_1.set_duty(0)
                self.state1.write(0)
        
        if (state == 2):
            if (self.boolean2 == True):
                self.boolean2 = False
                self.motor_2.set_duty(0)
                self.state2.write(0)
            else:
                # Simply transition to state 0 if data collection did not start yet.
                self.motor_2.set_duty(0)
                self.state2.write(0)