""" @file               BNO055.py
    @brief              IMU driver for motor 
    @details            
    @author             Damond Li
    @author             Chris Or
    @date               October 19, 2021
"""
import pyb
from pyb import I2C
import struct

class BNO055:
    """ @brief                  
        @details                
    """
    
    def __init__(self, number):
        """ @brief              
        """
        
        ## @brief  Serial port object
        self.SERPORT = pyb.USB_VCP()
        
        self.Master = I2C(number, mode=I2C.MASTER)
        self.Slave = I2C(I2C.SLAVE, addr=0x28)
        
        self.set_unit = 0b00000000
        self.Master.mem_write(self.set_unit, 0x28, 0x3B)
        
        print("Currently in Configuration Mode.")
               
    def change_mode(self, BNmode):
        """ @brief              
            @details            
        """
        if (BNmode == "ACCONLY"):
            buf = 0b00000001
        elif(BNmode == "MAGONLY"):
            buf = 0b00000010
        elif(BNmode == "GYROONLY"):
            buf = 0b00000011
        elif(BNmode == "ACCMAG"):
            buf = 0b00000100
        elif(BNmode == "ACCGYRO"):
            buf = 0b00000101
        elif(BNmode == "MAGGYRO"):
            buf = 0b00000110
        elif(BNmode == "AMG"):
            buf = 0b00000111
        elif(BNmode == "NDOF"):
            buf = 0b00001100
        elif(BNmode == "CONFIGMODE"):
            buf = 0b00000000

        self.Master.mem_write(buf, 0x28, 0x3D)
                
    def get_calibration_status(self):
        """ @brief              
            @return             
        """
        buf = bytearray(1)
        byte = self.Master.mem_read(buf, 0x28, 0x35)
        cal_status = (byte[0] & 0b11,
                      (byte[0] & 0b11 << 2) >> 2,
                      (byte[0] & 0b11 << 4) >> 4,
                      (byte[0] & 0b11 << 6) >> 6)

        return "Calibration Status: " + str(cal_status)
    
    def get_calibration_coeff(self):
        """ @brief             
        """
        buf = bytearray(22)
        self.cal_coeff = self.Master.mem_read(buf, 0x28, 0x55)
        return ("Calibration Coefficients:\n" + 
                str(struct.unpack('<hhhhhhhhhhh', self.cal_coeff)))
        
    def write_calibration_coeff(self):
        """ @brief                           
        """
        self.Master.mem_write(self.cal_coeff, 0x28, 0x55)
        
    
    def get_euler(self):
        # Head, Roll, Pitch
        self.euler_hex = BN.Master.mem_read(6, 0x28, 0x1A)
        self.euler_int = list(struct.unpack('<hhh', self.euler_hex))
        self.corrected_euler = []
        
        for idx in range(len(self.euler_int)):
            self.corrected_euler.append(float(self.euler_int[idx]) / 16)
            
        return "Scaled Euler Angles (deg): " + str(self.corrected_euler)
        
    
    def get_omega(self):
        self.omega_hex = BN.Master.mem_read(6, 0x28, 0x14)
        self.omega_int = list(struct.unpack('<hhh', self.omega_hex))
        self.corrected_omega = []
        
        for idx in range(len(self.omega_int)):
            self.corrected_omega.append(float(self.omega_int[idx]) / 16)
            
        return "Scaled Angular Velocity (dps): " + str(self.corrected_omega)
        
        
    def get_accel(self):
        # Get X
        x_hex = BN.Master.mem_read(2, 0x28, 0x08)
        x_int = struct.unpack('<h', x_hex)
        # Get Y
        y_hex = BN.Master.mem_read(2, 0x28, 0x0A)
        y_int = struct.unpack('<h', y_hex)
        # Get Z
        z_hex = BN.Master.mem_read(2, 0x28, 0x0C)
        z_int = struct.unpack('<h', z_hex)
        return tuple([x_int, y_int, z_int])
    
    def update(self):
        
        if(self.SERPORT.any()):
           USER_IN = self.SERPORT.read(1)
           
           if(USER_IN == b'0'):
               self.change_mode("CONFIGMODE")
               print("Entering Config Mode")
           elif (USER_IN == b'1'):
               self.change_mode("ACCONLY")
               print("Entering Accelerometer Only Mode")
           elif (USER_IN == b'2'):
               self.change_mode("MAGONLY")
               print("Entering Magnetometer Mode")
           elif (USER_IN == b'3'):
               self.change_mode("GYROONLY")
               print("Entering Gyro Mode")
           elif (USER_IN == b'4'):
               self.change_mode("ACCMAG")
               print("Entering Accelerometer and Magnetometer Mode")
           elif (USER_IN == b'5'):
               self.change_mode("ACCGYRO")
               print("Entering Accelerometer and Gyro Mode")
           elif (USER_IN == b'6'):
               self.change_mode("MAGGYRO")
               print("Entering Entering Magnetometer and Gyro Mode")
           elif (USER_IN == b'7'):
               self.change_mode("AMG")
               print("Entering AMG Mode")
           elif (USER_IN == b'8'):
               self.change_mode("NDOF")
               print("Entering NDOF Mode")
               
           elif (USER_IN == b'c'):
               self.write_calibration_coeff()
           elif (USER_IN == b'g'):
               print(self.get_calibration_coeff())
           elif (USER_IN == b'w'):
               self.write_calibration_coeff()
           elif (USER_IN == b'z'):
               print(self.get_calibration_status())
           elif (USER_IN == b'e'):
               print(self.get_euler())
           elif (USER_IN == b'o'):
               print(self.get_omega())

                    
if __name__ == '__main__':
    if (input("Enter " " to begin") == " "):
        BN = BNO055(1)
        BN.change_mode("CONFIGMODE")
                
        while(True):
            try:
                BN.update()
                
            except KeyboardInterrupt:
                break
            
        print('Program Terminating')
        
        
