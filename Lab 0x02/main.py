# import pyb
import shares
import task_encoder
import task_user

# Start off at state = 0
STATE = shares.Share(0)
PRINT = shares.Queue()

if __name__ == '__main__':
        
    ## Create task object
    task_1 = task_encoder.Task_Encoder(period=10.00, state=STATE, UI_print=PRINT)
    task_2 = task_user.Task_User(state=STATE, UI_print=PRINT) 
    
    while(True):
        
        try:
            task_1.run()
            task_2.update()
            
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')
