""" @file               task_user.py
    @brief
    @details
    @author
    @date
"""

# period = 65535
# prescaler = 0
# class for encoder driver, encoder, UI
import pyb

SERPORT = pyb.USB_VCP()

# The main program should run continuously
class Task_User():
        
    def __init__(self, state, UI_print):
        self.state = state
        self.UI_print = UI_print
        print('Welcome! Pressing z will zero encoder 1,'
             ' pressing p will print out the position of encoder 1,'
             ' pressing d will print out the delta for encoder 1,'
             ' pressing g will collect 30s of encoder data and print to'
             ' PuTTY as a comma separated list, and pressing s will' 
             ' end the data collection prematurely.')
        
    def update(self):
        
        # Check which state the system is in
        if(self.state.read() == 0):
            
           # Read only when in State 0
           if(SERPORT.any()):
               USER_IN = SERPORT.read(1)
               
               # Update the state by writing the corresponding state
               if(USER_IN == b'z'):
                   self.state.write(1)
               elif (USER_IN == b'p'):
                   self.state.write(2)
               elif (USER_IN == b'd'):
                   self.state.write(3)
               elif (USER_IN == b'g'):
                   self.state.write(4) 
               elif (USER_IN == b's'):
                   print('I Pressed S')
                   self.state.write(5) 
            
        # Check if there is something in queue to print
        if (self.UI_print.num_in != 0):
            for idx in range(self.UI_print.num_in()):
                print(self.UI_print.get())

