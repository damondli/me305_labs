""" @file               encoder.py
    @brief
    @details
    @author
    @date
"""

# period = 65535
# prescaler = ???
# class for encoder driver, encoder, UI
import pyb

class Encoder:
    """ @brief                  Interface with quadrature encoders
        @details
    """
    
    def __init__(self):
        """ @brief              Constructs an encoder object
            @details
        """
        self.pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
        self.pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
        self.tim4 = pyb.Timer(4, prescaler = 0, period = 65535)
        self.t4ch1 = self.tim4.channel(1, pyb.Timer.ENC_AB, pin = self.pinB6)
        self.t4ch2 = self.tim4.channel(2, pyb.Timer.ENC_AB, pin = self.pinB7)
        self.pos_1 = self.tim4.counter()
        self.pos_2 = self.tim4.counter()
        self.position = self.tim4.counter()
        
    def update(self):
        """ @brief              Updates encoder position and delta
            @details
        """
        
        # Establish current and previous previous to determine delta
        self.pos_1 = self.pos_2
        self.pos_2 = self.tim4.counter()
        delta_counter = self.pos_2 - self.pos_1
        
        # Account for the overflow or auto-reload
        if delta_counter > 55000:
            self.corrected_delta = delta_counter - 65535
        elif delta_counter < -55000:
            self.corrected_delta = delta_counter + 65535
        else:
            self.corrected_delta = delta_counter
            
        # Update the position accordingly
        self.position += self.corrected_delta
        
    def get_position(self):
        """ @brief              Returns encoder position
            @return             The position of the encoder shaft
        """
        return self.position
    
    def set_position(self, position):
        """ @brief              Sets encoder position
            @param  position    The new position of the encoder shaft
        """
        print()
        
    def get_delta(self):
        """ @brief              Gets the corrected change in position
            @return             The change in position of the encoder shaft
                                between the two most recent updates
        """
        return self.corrected_delta

            