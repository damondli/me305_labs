""" @file               task_user.py
    @brief              User interface task implementation
    @details            This task is responsible for reading and writing the
                        the user input through the virtual com port.
    @author             Damond Li
    @author             Chris Or
    @date               December 8, 2021
"""

import pyb

## @brief  Serial port object
SERPORT = pyb.USB_VCP()

class Task_User():
    """ @brief                  Class responsible for interfacing with user
        @details                This file is responsible for handling all user
                                I/O's. The data will be retrieved from and
                                written to the encoder task through the shares
                                class.
    """

    def __init__(self, state):
        """ @brief                      Constructs a user interface task
            @details                    The user task is implemented as a finite
                                        state machine.
            @param      state           A share. Share object describing the state of
                                        the finite state machine.
        """
        print('Welcome to our ball balancing platform project!'
              ' \n '
              ' \n Please use the following instructions to get started:'
              ' \n '
              ' \n      1) Press "b" to start the balancing process. Note:the '
              ' \n         motors are disabled until the touch panel senses an'
              ' \n         object on it. '
              ' \n      2) Place the 40mm diameter bearing ball firmly in the '
              ' \n         center of the marked touch panel for a for a couple' 
              ' \n         of seconds until noticeably stable. '
              ' \n      3) Let go of the bearing ball and watch it balance!!'  
              ' \n '
              ' \n To stop the balancing or shut off motors at any time, press "s"'
              ' \n '
              ' \n If you require the instructions again, please press "h" for help')

        ## @brief A shares.Share object for the state of the finite state machine
        self.state = state

    def update(self):
        """ @brief          Runs one iteration of the finite state machine.
            @details        Runs a single iteration of the described finite state machine.
                            This task is updated at the fastest speed possible with no specified
                            frequency.
        """
        if(SERPORT.any()):
           ## @brief An object that reads the first byte from the VCP
           USER_IN = SERPORT.read(1)
           
           # Update the state by writing the corresponding state
           if(USER_IN == b's' or USER_IN == b'S'):
               self.state.write(1)
               print('Balancing process terminated')
           elif (USER_IN == b'b' or USER_IN == b'B'):
               self.state.write(2)
               print('Balancing process initiated')
           elif (USER_IN == b'h' or USER_IN == b'H'):
               print(' \nPlease use the following commands:'
                     ' \nb:     Start balancing process'
                     ' \ns:     Stop the balancing or shut off motors'
                     ' \nh:     Repeat command options')


