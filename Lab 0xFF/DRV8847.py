""" @file               DRV8847.py
    @brief              Motor driver 
    @details            This motor driver is responsible for interfacing
                        with the motor. This driver creates the motor objects that
                        will be used in the motor task.
    @author             Damond Li
    @author             Chris Or
    @date               December 8, 2021
"""

import pyb

class DRV8847:
    """ @brief                  Interface with the DRV8847 motor
        @details                This class will be referenced in the motor 
                                task. This class deals with enabling/disabling
                                the motor as well as handling faults and determining
                                which motor is being operated
    """
    
    def __init__ (self):
        """ @brief              Initializes and returns a DRV8847 object.
            @details            Establishes pin objects to initialize the
                                DRV8847 motor
        """
        ## @brief Object for PinA15
        self.pinA15 = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        ## @brief Object for PinB2
        self.pinB2 = pyb.Pin(pyb.Pin.cpu.B2)

    def motor (self, motor_select):
        """ @brief              Initializes and returns a motor object associated with the DRV8847.
            @details            Creates a motor object through the DRV8847 class
            @param              motor_select    Designates which motor is being used
            @return             An object of class Motor
        """
        return Motor(motor_select)
    
class Motor:
    """ @brief                  A motor class for one channel of the DRV8847.
        @details                Objects of this class can be used to apply PWM
                                to a given DC motor.
    """
    
    def __init__(self, motor_select):
        """ @brief              Initializes and returns a motor object associated with the DRV8847.
            @details            Objects of this class should not be instantiated 
                                directly. Instead create a DRV8847 object and
                                use that to create Motor objects using the method 
                                DRV8847.motor().
            @param              motor_select    Designates which motor is being used
        """
        if (motor_select == 1):
            ## @brief Object for PinB4
            self.pinB4 = pyb.Pin(pyb.Pin.cpu.B4)
            ## @brief Object for PinB5
            self.pinB5 = pyb.Pin(pyb.Pin.cpu.B5)
            ## @brief Establish the frequency for the timer
            self.tim3 = pyb.Timer(3, freq = 20000)
            ## @brief Object for timer 3 channel 1
            self.timer1 = self.tim3.channel(1, pyb.Timer.PWM, pin = self.pinB4)
            ## @brief Object for timer 3 channel 2
            self.timer2 = self.tim3.channel(2, pyb.Timer.PWM, pin = self.pinB5)    
        elif (motor_select == 2):
            ## @brief Object for PinB0
            self.pinB0 = pyb.Pin(pyb.Pin.cpu.B0)
            ## @brief Object for PinB1
            self.pinB1 = pyb.Pin(pyb.Pin.cpu.B1)
            ## @brief Establish the frequency for the timer
            self.tim3 = pyb.Timer(3, freq = 20000)
            ## @brief Object for timer 3 channel 3
            self.timer1 = self.tim3.channel(3, pyb.Timer.PWM, pin = self.pinB0)
            ## @brief Object for timer 3 channel 1
            self.timer2 = self.tim3.channel(4, pyb.Timer.PWM, pin = self.pinB1)
                
        self.timer1.pulse_width_percent(0)
        self.timer2.pulse_width_percent(0)
 
    def set_duty (self, duty):
        """ @brief              Set the PWM duty cycle for the motor channel.
            @details            This method sets the duty cycle to be sent
                                to the motor to the given level. Positive values
                                cause effort in one direction, negative values 
                                in the opposite direction. 
            @param              duty    A signed number holding the duty cycle
                                        of the PWM signal sent to the motor.
        """
        if (duty >= 0):
            self.timer1.pulse_width_percent(duty)
            self.timer2.pulse_width_percent(0)
        else:
            self.timer1.pulse_width_percent(0)
            self.timer2.pulse_width_percent(abs(duty))


    
    
    
        
        
        
        