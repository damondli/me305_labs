""" @file               task_motor.py
    @brief              Motor task for the motor driver.
    @details            This task is responsible for controlling the duty cycle
                        of the PDM going to the motor. This duty cycle will be
                        coming from a shares object written by the task controller.
    @author             Damond Li
    @author             Chris Or
    @date               December 8, 2021
"""

import DRV8847

class Task_Motor:
    """ @brief                  Interface with two DR8847 motors
        @details                This class has one main function that runs the two motors.
    """

    def __init__(self, dutyX, dutyY, state):
        """ @brief                  Constructs a motor driver object and creates two motor objects.
            @details                The contructor function creates a motor driver object
                                    and creates two motor objects from the motor driver.
            @param  dutyX           A shares. Share object describing the duty cycle
                                    to run the motor responsible for controlling the x direction.
            @param  dutyY           A shares. Share object describing the duty cycle
                                    to run the motor responsible for controlling the y direction.
        """
        ## @brief This object is the motor driver
        self.motor_drv = DRV8847.DRV8847()
        
        ## @brief This object is the first motor constructed from the driver
        self.motor1 = DRV8847.Motor(1)
        ## @brief This object is the second motor constructed from the driver
        self.motor2 = DRV8847.Motor(2)
        
        ## @brief This object is a shares object describing the duty cycle to run motor 1
        self.dutyX = dutyX
        ## @brief This object is a shares object describing the duty cycle to run motor 2
        self.dutyY = dutyY

        ## @brief This object is a shares object describing the state of the finite state machine
        self.state = state
        
        ## @brief This value is used to quickly enable and disable the motors, primarily for debugging.
        self.motorboolvalue = 0


    def run(self):
        """ @brief              Updates each motor to run at the specified duty cycle.
            @details            This function reads from the duty cycle shares object.
                                The data from the duty cycle is then used to run a method
                                of the motor driver class to set the duty cycle.
        """
        if (self.state.read() == 2):
            self.enable()
        elif (self.state.read() == 1):
            self.disable()

        self.motor1.set_duty(-1 * self.motorboolvalue * self.dutyX.read())
        self.motor2.set_duty(-1 * self.motorboolvalue * self.dutyY.read())

    def enable(self):
        """ @brief              This method enables the motors
            @details            This changes a value that is multiplied to the duty cycle. This multiplier is "1" in this method.
        """
        self.motorboolvalue = 1

    def disable(self):
        """ @brief              This method disables the motors
            @details            This changes a value that is multiplied to the duty cycle. This multiplier is "0" in this method.
        """
        self.motorboolvalue = 0