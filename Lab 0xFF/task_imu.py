""" @file               task_imu.py
    @brief              IMU task for the BNO055 IMU driver
    @details            This task is responsible for using the BNO055 IMU driver
                        to obtain values for the platform tilt and rotational 
                        velocity in both the x and y directions. The values used 
                        are based on the coordinate system written on the touch 
                        panel surface and not the designated coordinates from the
                        BNO055 manual.
    @author             Damond Li
    @author             Chris Or
    @date               December 8, 2021
"""

import BNO055

class Task_IMU():
    def __init__(self, thetax, thetay, thetadx, thetady):
        """ @brief                  Constructs an IMU driver object and creates
                                    objects for values for x and y coordinates.
            @details                Constructs an IMU driver object and creates
                                    objects for values for x and y coordinates.
                                    Also sets the mode of the IMU to NDOF mode 
                                    to allow for the calibration of the IMU and 
                                    the collection of data.
            @param  thetax          A shares. Share object describing the tilt 
                                    of the platform about the x axis as described
                                    by the touch panel.
            @param  thetay          A shares. Share object describing the tilt 
                                    of the platform about the y axis as described
                                    by the touch panel.
            @param  thetadx         A shares. Share object describing the rotational
                                    velocity of the platform about the x axis as 
                                    described by the touch panel.
            @param  thetady         A shares. Share object describing the rotational
                                    velocity of the platform about the y axis as 
                                    described by the touch panel.
        """

        ## @brief Object for the IMU driver
        self.IMU = BNO055.BNO055(1)
        # Change the IMU to the NDOF mode
        self.IMU.change_mode("NDOF")
        ## @brief A shares object for the angular position in the X direction
        self.theta_x = thetax
        ## @brief A shares object for the angular position in the Y direction
        self.theta_y = thetay
        ## @brief A shares object for the angular velocity in the X direction
        self.theta_dot_x = thetadx
        ## @brief A shares object for the angular velocity in the Y direction
        self.theta_dot_y = thetady
        
        
    def update(self):
        """ @brief              Updates to read current data from IMU and write
                                to the controller in required units.
            @details            This function updates to read Euler angle and 
                                angular velocity data from the IMU and write this
                                data to the corresponding variables and units to
                                the controller task.
        """

        ## @brief A tuple with the angular positions
        self.euler = self.IMU.get_euler()
        self.theta_y.write(-1 * self.euler[2]*(3.1415926535898/180))
        self.theta_x.write(-1 * self.euler[1]*(3.1415926535898/180))

        ## @brief A tuple with the angular velocities
        self.omega = self.IMU.get_omega()
        self.theta_dot_x.write(self.omega[1]*(3.1415926535898/180))
        self.theta_dot_y.write(-1 * self.omega[0]*(3.1415926535898/180))
