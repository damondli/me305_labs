""" @file               BNO055.py
    @brief              IMU driver
    @details            This IMU driver is responsible for interfacing with the
                        IMU used for the term project. The driver allows for 
                        calibration and data collection for the IMU.
    @author             Damond Li
    @author             Chris Or
    @date               December 8, 2021
"""
import pyb
import os
from pyb import I2C
import struct

class BNO055:
    """ @brief                  Interface with the BNO055 inertial measurement unit
        @details                This class will be referenced in the IMU
                                task. This class deals with the calibration of the IMU and reading the calibration
                                coefficients from the text file on the Nucleo board. This class also deals with the
                                changing of IMU modes as well as the euler angles, velocities, and accelerations.
                                The finite state machine for the earlier calibration and data tasks are handled in the
                                update function
    """
    
    def __init__(self, number):
        """ @brief              Initializes and returns a BNO055 object.
            @details            Reads and writes calibration coefficients from text file to and from the designated
                                locations
            @param  number      A share. Share object describing the location for the master bus
        """

        # Calibration coefficients are read from the text file already on the Nucleo board
        filename = "IMU_cal_coeffs.txt"
        if filename in os.listdir():
            # File exists, read from it
            with open(filename, 'r') as f:
                cal_data_string = f.readline()
                cal_values = [int(str(cal_value), 16) for cal_value in cal_data_string.strip().split(',')]
                self.cal_coeff = bytearray()
                for idx in cal_values:
                    self.cal_coeff.append(idx)
        else:
            pass
        
        ## @brief  Serial port object
        self.SERPORT = pyb.USB_VCP()

        ## @brief  Object for the master serial bus
        self.Master = I2C(number, mode=I2C.MASTER)
        ## @brief  Object for the slave serial bus
        self.Slave = I2C(I2C.SLAVE, addr=0x28)
        ## @brief  Object to set units of output data
        self.set_unit = 0b00000000

        self.Master.mem_write(self.set_unit, 0x28, 0x3B)
        self.write_calibration_coeff()
               
    def change_mode(self, BNmode):
        """ @brief              Change mode of IMU
            @details            This function changes the mode of the IMU to enable various components. The modes are a
                                combination of the accelerometer, magnetometer, and gyroscope and a configuration mode.
        """
        if (BNmode == "ACCONLY"):
            buf = 0b00000001
        elif(BNmode == "MAGONLY"):
            buf = 0b00000010
        elif(BNmode == "GYROONLY"):
            buf = 0b00000011
        elif(BNmode == "ACCMAG"):
            buf = 0b00000100
        elif(BNmode == "ACCGYRO"):
            buf = 0b00000101
        elif(BNmode == "MAGGYRO"):
            buf = 0b00000110
        elif(BNmode == "AMG"):
            buf = 0b00000111
        elif(BNmode == "NDOF"):
            buf = 0b00001100
        elif(BNmode == "CONFIGMODE"):
            buf = 0b00000000

        self.Master.mem_write(buf, 0x28, 0x3D)
                
    def get_calibration_status(self):
        """ @brief              Retrieve calibration status from IMU              
            @details            This function returns the current calibration status of the IMU
                                for the 3 components and the IMU system as a whole. For each of these
                                4 items, the value of 3 means a the item is fully calibrated. 
        """
        buf = bytearray(1)
        byte = self.Master.mem_read(buf, 0x28, 0x35)
        cal_status = (byte[0] & 0b11,
                      (byte[0] & 0b11 << 2) >> 2,
                      (byte[0] & 0b11 << 4) >> 4,
                      (byte[0] & 0b11 << 6) >> 6)

        return "Calibration Status: " + str(cal_status)
    
    def get_calibration_coeff(self):
        """ @brief              Retrieve calibration coefficients from IMU              
            @details            This function returns the current calibration coefficients from the IMU as
                                hex values in a bytearray to be used in the calibration process.
        """
        buf = bytearray(22)
        self.cal_coeff = self.Master.mem_read(buf, 0x28, 0x55)
        return self.cal_coeff

        
    def write_calibration_coeff(self):
        """ @brief              Write calibration coefficients to the Master bus                                       
        """
        self.Master.mem_write(self.cal_coeff, 0x28, 0x55)
        
    
    def get_euler(self):
        """ @brief              Retrieve Euler angles from IMU              
            @details            This function returns the Euler angles in all directions from the IMU.
                                These include the head, roll, and pitch values at a given time.
        """
        # Head, Roll, Pitch
        self.euler_hex = self.Master.mem_read(6, 0x28, 0x1A)
        self.euler_int = list(struct.unpack('<hhh', self.euler_hex))
        self.corrected_euler = []
        
        for idx in range(len(self.euler_int)):
            self.corrected_euler.append(float(self.euler_int[idx]) / 16)
            
        return self.corrected_euler
        
    
    def get_omega(self):
        """ @brief              Retrieve angular velocities in all directions from IMU             
            @details            This function returns the angular velocities in all directions 
                                from the IMU.
        """
        # X,Y,Z
        self.omega_hex = self.Master.mem_read(6, 0x28, 0x14)
        self.omega_int = list(struct.unpack('<hhh', self.omega_hex))
        self.corrected_omega = []
        
        for idx in range(len(self.omega_int)):
            self.corrected_omega.append(float(self.omega_int[idx]) / 16)
            
        return self.corrected_omega
        
        
    def get_accel(self):
        """ @brief              (unused) Retrieve accelerations in all directions from the IMU            
            @details            (unused) This function returns the accelerations in all directions from the IMU.
        """
        # Get X
        x_hex = BN.Master.mem_read(2, 0x28, 0x08)
        x_int = struct.unpack('<h', x_hex)
        # Get Y
        y_hex = BN.Master.mem_read(2, 0x28, 0x0A)
        y_int = struct.unpack('<h', y_hex)
        # Get Z
        z_hex = BN.Master.mem_read(2, 0x28, 0x0C)
        z_int = struct.unpack('<h', z_hex)
        return tuple([x_int, y_int, z_int])
    
    def update(self):
        """ @brief          Runs one iteration of the finite state machine for Lab0x05.
            @details        Runs a single iteration of the described finite state machine.
                            This task is updated at the fastest speed possible with no specified
                            frequency. This function is used in running the IMU task solely for 
                            calibration purposes.
        """
        if(self.SERPORT.any()):
           USER_IN = self.SERPORT.read(1)
           
           if(USER_IN == b'0'):
               self.change_mode("CONFIGMODE")
               print("Entering Config Mode")
           elif (USER_IN == b'1'):
               self.change_mode("ACCONLY")
               print("Entering Accelerometer Only Mode")
           elif (USER_IN == b'2'):
               self.change_mode("MAGONLY")
               print("Entering Magnetometer Mode")
           elif (USER_IN == b'3'):
               self.change_mode("GYROONLY")
               print("Entering Gyro Mode")
           elif (USER_IN == b'4'):
               self.change_mode("ACCMAG")
               print("Entering Accelerometer and Magnetometer Mode")
           elif (USER_IN == b'5'):
               self.change_mode("ACCGYRO")
               print("Entering Accelerometer and Gyro Mode")
           elif (USER_IN == b'6'):
               self.change_mode("MAGGYRO")
               print("Entering Entering Magnetometer and Gyro Mode")
           elif (USER_IN == b'7'):
               self.change_mode("AMG")
               print("Entering AMG Mode")
           elif (USER_IN == b'8'):
               self.change_mode("NDOF")
               print("Entering NDOF Mode")
               
           elif (USER_IN == b'c'):
               self.write_calibration_coeff()
           elif (USER_IN == b'g'):
               print(self.get_calibration_coeff())
           elif (USER_IN == b'w'):
               self.write_calibration_coeff()
           elif (USER_IN == b'z'):
               print(self.get_calibration_status())
           elif (USER_IN == b'e'):
               print(self.get_euler())
           elif (USER_IN == b'o'):
               print(self.get_omega())
        
        
