''' @file                mainfile.py
    @brief               Main file for ME305 documentation
    @details             This is the main file for the site created through Doxygen to document work completed in ME305,
                        Introduction to Mechatronics. The site includes information and some code for each project.
    @mainpage

    @section sec_intro   Introduction
                         This is the documentation site for  Damond Li's  work in ME 305 - Introduction to
                         Mechatronics in the Fall 2021 Quarter <br><br>
                         Source code for all lab assignments is available <a href="https://bitbucket.org/damondli/me305_labs/src/master/">HERE</a>

    @page    page1       Lab 0xFF - Term Project
    \tableofcontents

    \section sec1 Video Demonstration

    Video Demonstration of Controls and Operation of Ball-Balancing Platform:<br>
    \htmlonly
    <iframe width="560" height="315" src="https://www.youtube.com/embed/53w_fSKBNms" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    \endhtmlonly

    Video Demonstration of Initial Testing:<br>
    \htmlonly
    <iframe width="560" height="315" src="https://www.youtube.com/embed/o59lStqicl8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    \endhtmlonly

    \section sec2 Approach
    The first step taken for this term project was to create a task diagram describing how <br>
    our program operates. Please refer to Figure 1 for the task diagram. From the task <br>
    diagram, there were 5 main variables that will be shared across the different tasks: <br>
    linear position (X and Y), linear velocity (X and Y), angular position (pitch and roll),<br>
    angular velocity (pitch and roll), and duty cycle. The resistive touch panel is <br>
    responsible for the linear positions and velocities. The IMU is responsible for the <br>
    angular positions and velocities. The controller is responsible for determining the duty <br>
    cycle. With that in mind, it would then be a matter of getting each task to output their <br>
    respective data. The finite state machine implemented for our user task is shown in Figure 2. <br>

    \subsection subsection1 Task Diagram
    \image html TermTD.png "Figure 1: Term Project Task Diagram"

    \subsection subsection2 Finite State Machine
    \image html TermFSM.png "Figure 2: Term Project Finite State Machine"

    \section sec3 Modeling

    To model our system, we analyzed the kinematics and kinetics of two systems. The ball <br>
    rolling without slip, and the ball and platform as a whole. We then found the equations <br>
    of motion of the system that are used in the Simulation section. The hand calculations <br>
    used for modeling are shown below: <br>

    @image  html                1001.JPG
    @image  html                1002.JPG
    @image  html                1003.JPG
    @image  html                1004.JPG
    @image  html                1005.JPG

    \section sec4 Simulation

    Below are 3 versions of the same MATLAB livescript used to complete the MATLAB simulation <br>
    for HW0x03, each with different sets of plots as specified. <br>
    Click in the MATLAB Table of Contents for each version to see the corresponding plots.
    Everything other than the plots is the same in all three files. The corresponding
    discussions for each step are also included in the MATLAB Table of Contents. <br><br>
    Please click on the following to see the full MATLAB Simulation:<br><br>
    <A HREF="ME305_HW0x03.html"> MATLAB Simulation </A> <br>

    \section sec5 Determining Gains
    To determine the gains to run our full state feedback controller, a MATLAB script based <br>
    off of HW0x03. It was determined that it would be extremely difficult to balance one of <br>
    the smaller ball bearings due to its small mass. When the smaller ball bearings roll <br>
    across the touch panel as the platform is moving, there are instances where the ball <br>
    lifts off from the platform which can be problematic for the controller. A larger ball <br>
    bearing with a larger mass is necessary in order to maintain constant contact with the <br>
    touch panel. Accordingly, the MATLAB script from HW0x03 was updated with the larger ball <br>
    mass and radius to determine the open-loop Jacobian matrix. A closed-loop matrix was <br>
    derived by manipulating the open-loop matrices. The poles of the closed-loop matrix was <br>
    chosen so that the closed-loop response will have a natural frequency of 8.2, a damping <br>
    ratio of 0.8, and two poles at -16 and -17. The gain values were then calculated with the <br>
    desired response. Please click <A HREF="GainCalcHTML.html">HERE</A> for the MATLAB <br>
    script used for this calculation.

    \section sec6 Updated Tasks
    For the term project, many of the tasks from Lab 0x01, 0x02, 0x03, and 0x04 were updated <br>
    to only contain the necessary methods to run our ball and platform system. For example, <br>
    task_motor.py previously contained methods to run the motors at a specified speed and <br>
    gain value for 10 seconds. That method has been removed from task_motor.py for the term <br>
    project. In addition, the hardware for the motor has changed in that the motor is able to <br>
    output three times the torque and the motor can no longer fault.<br>

    \section sec7 Important Observations
    In the middle of debugging the program, it was observed that the program had no problem <br>
    balancing the ball in one of the two directions. It was later observed that the source<br>
    of the problem is due to the specified coordinate system. In one direction, a positive <br>
    rotation causes the ball to accelerate in the positive direction. However, in the <br>
    perpendicular direction, a positive rotation causes the ball to accelerate in the <br>
    negative direction. The full state feedback controller is designed for the former. In <br>
    order to resolve this issue, the positional data was simply multiplied by a negative <br>
    before entering the full state feedback controller. <br>

    @author                      Damond Li
    @author                      Christopher Or
    @date                        December 8, 2021

    @page    page7       Lab 0x00 - Fibonacci

    \tableofcontents

    \section sec20 Lab 0x00 Source Code

    Lab 0x00 source code is available <a href="https://bitbucket.org/damondli/me305_labs/src/master/Lab%200x00/">HERE</a>

    @page    page2       Lab 0x01 - LEDs

    \tableofcontents

    \section sec10 Lab 0x01 Source Code

    Lab 0x01 source code is available <a href="https://bitbucket.org/damondli/me305_labs/src/master/Lab%200x01/">HERE</a>

    \section sec8 Lab 0x01 Video Demonstration

    Video Demonstration of Light Patterns for Lab 0x01:<br>
    \htmlonly
    <iframe width="560" height="315" src="https://www.youtube.com/embed/lrsif5nEs00" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    \endhtmlonly

    \section sec9 Lab 0x01 Finite State Machine

    \image html Lab1FSM.png "Lab 0x01 State Transition Diagram for Finite State Machine"

    @page    page3       Lab 0x02 - Incremental Encoders

    \tableofcontents

    \section sec11 Lab 0x02 Source Code

    Lab 0x02 source code is available <a href="https://bitbucket.org/damondli/me305_labs/src/master/Lab%200x02/">HERE</a>

    \section sec12 Lab 0x02 Task Diagram
    \image html Lab2TD.png "Lab 0x02 Task Diagram"

    \section sec13 Lab 0x02 Finite State Machine
    \image html Lab2FSM.png "Lab 0x02 State Transition Diagram for Finite State Machine"

    @page    page4       Lab 0x03 - PMDC Motors

    \tableofcontents

    \section sec14 Lab 0x03 Source Code

    Lab 0x03 source code is available <a href="https://bitbucket.org/damondli/me305_labs/src/master/Lab%200x03/">HERE</a>

    \section sec15 Lab 0x03 Plots
    \image html Lab3Plot1.png "Lab 0x03 Motor 1 Performance Plot (80% Duty Cycle)"
    \image html Lab3Plot2.png "Lab 0x03 Motor 2 Performance Plot (80% Duty Cycle)"

    @page    page5       Lab 0x04 - Closed-Loop Control

    \tableofcontents

    \section sec16 Lab 0x04 Source Code

    Lab 0x04 source code is available <a href="https://bitbucket.org/damondli/me305_labs/src/master/Lab%200x04/">HERE</a>

    \section sec17 Lab 0x04 Block Diagram and Task Diagram
    \image html Lab4BDTD.jpg "Lab 0x04 Block Diagram and Task Diagram"

    \section sec18 Lab 0x04 Finite State Machine
    \image html Lab4FSM1.jpg "Lab 0x04 State Transition Diagram for Motor Task"
    \image html Lab4FSM2.jpg "Lab 0x04 State Transition Diagrams for Encoder Task and User Task"

    \section sec20 Lab 0x04 Performance Plots
    \image html Lab4DCvT.png "Lab 0x04 Duty Cycle vs. Time Plot"
    \image html Lab4SvT.png "Lab 0x04 Speed vs. Time Plot"

    @page    page6       Lab 0x05 - IMUs

    \tableofcontents

    \section sec19 Lab 0x05 Source Code

    Lab 0x05 source code is available <a href="https://bitbucket.org/damondli/me305_labs/src/master/Lab%200x05/">HERE</a> <br>

    Note: No submission was required for Lab 0x05. Performance of the IMU driver was performed in an in-class demonstration.

'''