var searchData=
[
  ['sampletime_0',['sampletime',['../classtask__panel_1_1Task__Panel.html#a85c2391da3816b8f732c955f44a566a1',1,'task_panel::Task_Panel']]],
  ['serport_1',['SERPORT',['../classBNO055_1_1BNO055.html#a89e85fea3f18febb974483d754933499',1,'BNO055.BNO055.SERPORT()'],['../task__user_8py.html#a586be8f4da1716b341c68bc2c63970cb',1,'task_user.SERPORT()']]],
  ['set_5fduty_2',['set_duty',['../classDRV8847_1_1Motor.html#ae2e6c0feeb46de3f93c35e7f25a79a8b',1,'DRV8847::Motor']]],
  ['set_5funit_3',['set_unit',['../classBNO055_1_1BNO055.html#a68a4569f23f076310561798ad534156f',1,'BNO055::BNO055']]],
  ['share_4',['Share',['../classshares_1_1Share.html',1,'shares']]],
  ['shares_2epy_5',['shares.py',['../shares_8py.html',1,'']]],
  ['slave_6',['Slave',['../classBNO055_1_1BNO055.html#a040fa55f0703cef7c6c63874d1bd686d',1,'BNO055::BNO055']]],
  ['start_5fticks_7',['start_ticks',['../classtask__panel_1_1Task__Panel.html#a3a7b356b125b0b611e4995773275dd83',1,'task_panel::Task_Panel']]],
  ['state_8',['state',['../classtask__motor_1_1Task__Motor.html#acf2d68af196fc4df0590a3d746989465',1,'task_motor.Task_Motor.state()'],['../classtask__user_1_1Task__User.html#afefb79be360ac39f0ed9920de91f953e',1,'task_user.Task_User.state()']]]
];
