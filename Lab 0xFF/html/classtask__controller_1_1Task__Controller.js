var classtask__controller_1_1Task__Controller =
[
    [ "__init__", "classtask__controller_1_1Task__Controller.html#a7c0e9c4bede398aa8b1b36d2af923cdb", null ],
    [ "run", "classtask__controller_1_1Task__Controller.html#a736e5fe72379bd381b65603b08200e2e", null ],
    [ "dth", "classtask__controller_1_1Task__Controller.html#a1a8cd8e19a60a404ed01a7f6ab637ddd", null ],
    [ "duty", "classtask__controller_1_1Task__Controller.html#a75fbfc52b1be551a83e6946416055d22", null ],
    [ "dx", "classtask__controller_1_1Task__Controller.html#a15eb55a06b43c1a0515eed8c3cb6d73e", null ],
    [ "k1", "classtask__controller_1_1Task__Controller.html#a4d6a54092480fc667e182073fa990456", null ],
    [ "k2", "classtask__controller_1_1Task__Controller.html#abd198897a9cf6da20df9eb68884aedaf", null ],
    [ "k3", "classtask__controller_1_1Task__Controller.html#a6d2f134ded6b37f885ed49f067a3cc9c", null ],
    [ "k4", "classtask__controller_1_1Task__Controller.html#a357bf88278d56c9d4b99f00a86c8b233", null ],
    [ "K_t", "classtask__controller_1_1Task__Controller.html#a0db9eab680264e30bb5d678e4025e564", null ],
    [ "Resistance", "classtask__controller_1_1Task__Controller.html#a7ec11dd533568772a746d778392d4126", null ],
    [ "th", "classtask__controller_1_1Task__Controller.html#a1954d4839f1054f24b08fd6f359115f7", null ],
    [ "touchbool", "classtask__controller_1_1Task__Controller.html#af97a7f620a56f1f59f982988d918af46", null ],
    [ "V_dc", "classtask__controller_1_1Task__Controller.html#af59b0bd7600c2df0ecb62c1b9dd26893", null ],
    [ "x", "classtask__controller_1_1Task__Controller.html#afc4b99e42396a71174ab370d1e66cfc4", null ]
];