var classBNO055_1_1BNO055 =
[
    [ "__init__", "classBNO055_1_1BNO055.html#aa2540a05751c60ab4d1d1d5d83911488", null ],
    [ "change_mode", "classBNO055_1_1BNO055.html#a1c27e7791976041089b3ea0772213791", null ],
    [ "get_accel", "classBNO055_1_1BNO055.html#a1676a6454e0615f8a84ff5411811daa0", null ],
    [ "get_calibration_coeff", "classBNO055_1_1BNO055.html#af06124d01a721c06513c8e4af1d156d2", null ],
    [ "get_calibration_status", "classBNO055_1_1BNO055.html#aaa9dfe166380cf324009a27060cf00c7", null ],
    [ "get_euler", "classBNO055_1_1BNO055.html#a1873696ee7301f1188f5cdbbeb1d1900", null ],
    [ "get_omega", "classBNO055_1_1BNO055.html#a5cc089e83e452a99b718645047c07f43", null ],
    [ "update", "classBNO055_1_1BNO055.html#a841a8270bf9d2f8e3f509827d3ad00aa", null ],
    [ "write_calibration_coeff", "classBNO055_1_1BNO055.html#aefb4eb9c8476a6ff91415c854a92668c", null ],
    [ "Master", "classBNO055_1_1BNO055.html#a29fb5bdc270751974225b29c0e8ddfb7", null ],
    [ "SERPORT", "classBNO055_1_1BNO055.html#a89e85fea3f18febb974483d754933499", null ],
    [ "set_unit", "classBNO055_1_1BNO055.html#a68a4569f23f076310561798ad534156f", null ],
    [ "Slave", "classBNO055_1_1BNO055.html#a040fa55f0703cef7c6c63874d1bd686d", null ]
];