# -*- coding: utf-8 -*-

"""
Created on Tue Sep 21 12:39:01 2021
Author: Damond Li
Class: ME305-01

"""

def fib(idx):
    '''
    This function calculates the Fibonacci number at a user specified index
    '''
    
    # Create the array with the seed and the output value for index of zero
    value = 0
    array = [0,1]
    
    # If index is 1 then the output value is the second value in the array
    if idx == 1:
        value = array[1]
        return value
    
    # This is for index values greather than or equal to 2
    for n in range(idx - 1):
        # Add the first two values of the array and appen to the list
        value = array[n] + array[n + 1]
        array.append(value)
    
    return value

if __name__ == '__main__':
    # Prompt the user to input a value
    # Referenced: https://docs.python.org/3/tutorial/errors.html
    while True:
        try:
            # Check if the input value is a decimal
            idx = int(input('Please specify the index at which to calculate '
                            'the Fibonacci sequence: '))
            
            # Check if the input is greater than or equal to zero
            if idx >= 0:
                print ('Fibonacci number at '
                       'index {:} is {:}.'. format(idx, fib(idx)))
                break
            else:
                # Error Message
                print('Please enter an integer that is '
                      'greater than or qual to zero.')
                
        except ValueError:
            # Error Message
            print('Please enter an integer that is '
                  'greater than or qual to zero.')