""" @file               closedloop.py
    @brief              Encoder task for the encoder driver.
    @details            This task is responsible for establishing the frequency
                        at which data is collected from the encoder driver.
    @author             Damond Li
    @author             Chris Or
    @date               November 2, 2021
"""

import DRV8847
import task_encoder
import shares

class ClosedLoop:
    """ @brief
        @details
    """

    def __init__(self, omega_ref):
        """ @brief              Constructs an encoder task
            @details            The encoder task is implemented as a finite
                                state machine.
            @param  state       A share. Share object describing the state of
                                the finite state machine.
        """
        self.omega_ref = 0
        self.Kp = 0
        
    def update(self, omega):
        self.omega = omega
        self.error = self.omega_ref - self.omega
        self.output = self.error * self.Kp
        
        if (self.output >= 100):
            self.output = 100
        elif (self.output <= -100):
            self.output = -100
            
        return self.output
        
    def get_Kp(self):
        return self.Kp  
    
    def set_Kp(self, gain):
        self.Kp = gain
        
    def set_speed(self, speed):
        self.omega_ref = speed

        
if __name__ == '__main__':
    PRINT = shares.Queue()
    STATE = shares.Share(0)
    enctask = task_encoder.Task_Encoder(period=0.20, state=STATE, UI_print=PRINT, encodernum=2)
    
    motor_drv = DRV8847.DRV8847()
    motor_2 = motor_drv.motor(1)
    motor_2.set_duty(0)
    
    omega_ref = float(input('Set Omega'))
    CL = ClosedLoop(omega_ref)

    while(True):
        try:
            enctask.run()
            omega = enctask.get_omega()
            DUTY = CL.update(omega)
            motor_2.set_duty(DUTY)
            print(DUTY)
            print(omega)
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')
        
        