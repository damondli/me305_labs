""" @file               task_motor.py
    @brief              Motor task for the motor driver.
    @details            This task is responsible for establishing the frequency
                        at which data is collected from the encoder driver.
    @author             Damond Li
    @author             Chris Or
    @date               November 2, 2021
"""

import DRV8847
import utime

class Task_Motor:
    """ @brief                  Interface with two DR8847 motors
        @details                This class implements states required in the operation of the
                                motor
    """

    def __init__(self, state1, state2, duty_cycle1, duty_cycle2, speed1, speed2, desiredspeed1, desiredspeed2):
        """ @brief                  Constructs a motor task
            @details                The motor task is implemented as a finite
                                    state machine.
            @param  state           A shares.Share object describing the state of
                                    the finite state machine.
            @param  duty_cycle1     A shares.Queue object specifying the duty cycle
                                    for motor 1
            @param  duty_cycle2     A shares.Queue object specifying the duty cycle
                                    for motor 2
        """
        ## @brief Create motor object
        self.motor_drv = DRV8847.DRV8847()
        ## @brief Brings the DR8847 motor out of sleep mode
        self.motor_drv.enable()
        ## @brief Initiates motor 1
        self.motor_1 = self.motor_drv.motor(1)
        ## @brief Initiates motor 2
        self.motor_2 = self.motor_drv.motor(2)
        ## @brief Initializing duty cycle for motor 1
        self.motor_1.set_duty(0)
        ## @brief Initializing duty cycle for motor 2
        self.motor_2.set_duty(0)
        
        self.speed1 = speed1
        
        self.speed2 = speed2
        
        ## @brief Duty cycle for motor 1
        self.duty_cycle1 = duty_cycle1
        ## @brief Duty cycle for motor 2
        self.duty_cycle2 = duty_cycle2
        ## @brief A shares.Share object for the state of the finite state machine
        self.state1 = state1
        ## @brief A shares.Share object for the state of the finite state machine
        self.state2 = state2

    def run(self):
        """ @brief              Runs one iteration of the finite state machine.
            @details            This function calls to the motor driver
        """

        # State 7 Clears Fault
        if (self.state1.read() == 7 or self.state2.read() == 7):
            self.motor_drv.enable()
            self.state1.write(0) 
            self.state2.write(0)

        # State 5 sets duty cycle for motor 1
        if (self.state1.read() == 5):
            self.motor_1.set_duty(self.duty_cycle1.read())
            self.state1.write(0)

        # State 5 sets duty cycle for motor 2
        if (self.state2.read() == 5):
            self.motor_2.set_duty(self.duty_cycle2.read())
            self.state2.write(0)

