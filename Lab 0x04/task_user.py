""" @file               task_user.py
    @brief              User interface task implementation
    @details            This task is responsible for reading and writing the
                        the user input through the virtual com port.
    @author             Damond Li
    @author             Chris Or
    @date               November 2, 2021
"""

import pyb

## @brief  Serial port object
SERPORT = pyb.USB_VCP()

class Task_User():
    """ @brief                  Interface with user
        @details                This file is responsible for handling all user
                                I/O's. The data will be retrieved from and
                                written to the encoder task through the shares
                                class.
    """

    def __init__(self, tasknum, state, UI_print, duty_cycle, gain, desiredspeed):
        """ @brief                  Constructs a user interface task
            @details                The user task is implemented as a finite
                                    state machine.
            @param      state       A share. Share object describing the state of
                                    the finite state machine.
            @param      UI_print   A queue. Queue object containing the data to
                                    be printed over the VCP.
            @param      duty_cycle  A queue. Queue object containing the duty cycle
                                    to be read by the motor task
        """
        
        ## @brief A shares.Share object for the state of the finite state machine
        self.state = state
        
        self.UI_print = UI_print
               
        ## @brief a shares.Queue object for the duty cycle to be written to motor task
        self.duty_cycle = duty_cycle
        
        self.gain = gain
        
        self.desiredspeed = desiredspeed        
        
        ## @brief An object for the selected task
        self.tasknum = tasknum
        
        if (self.tasknum == 1):
            self.zeropos = b'z'
            self.printpos = b'p'
            self.delta = b'd'
            self.collectdata = b'g'
            self.printenc = 'Collecting data for encoder 1'
            self.setduty = b'm'
            self.printduty = 'Please enter an integer value between -100 and 100 for motor 1: '
            self.stepresponse = b'1'
        elif (self.tasknum == 2):
            self.zeropos = b'Z'
            self.printpos = b'P'
            self.delta = b'D'
            self.collectdata = b'G'
            self.printenc = 'Collecting data for encoder 2'
            self.setduty = b'M'
            self.printduty = 'Please enter an integer value between -100 and 100 for motor 2: '
            self.stepresponse = b'2'
        
    def update(self):
        """ @brief          Runs one iteration of the finite state machine.
            @details        Runs a single iteration of the described finite state machine.
                            This task is updated at the fastest speed possible with no specified
                            frequency.
        """
            
        if(SERPORT.any()):
           ## @brief An object that reads the first byte from the VCP
           USER_IN = SERPORT.read(1)
           
           # Update the state by writing the corresponding state
           if(USER_IN == self.zeropos):
               self.state.write(1)
           elif (USER_IN == self.printpos):
               self.state.write(2)
           elif (USER_IN == self.delta):
               self.state.write(3)
           elif (USER_IN == self.collectdata):
               self.state.write(4)
               print(self.printenc)
           elif (USER_IN == self.setduty):
               self.duty = int(input(self.printduty))
               self.duty_cycle.write(self.duty)
               self.state.write(5)
               
           elif (USER_IN == b's' or USER_IN == b'S'):
               self.state.write(6)
               print('Stopping data collection')
           elif (USER_IN == b'c' or USER_IN == b'C'):
               self.state.write(7)
               print('Clearing fault')
               
           elif (USER_IN == self.stepresponse):
               self.set_gain = input('Please enter a gain value:')
               self.set_speed = input('Please enter a speed value:')
               self.gain.put(self.set_gain)
               self.desiredspeed.write(self.set_speed)
               self.state.write(10)
        
        # Check if there is something in queue to print
        # If not State 4 (not collecting data), the data prints as entered.
        if (self.state.read() != 4): 
            if (self.UI_print.num_in() != 0):
                for idx in range(self.UI_print.num_in()):
                    print(self.UI_print.get())
        

