""" @file               task_encoder.py
    @brief              Encoder task for the encoder driver.
    @details            This task is responsible for establishing the frequency
                        at which data is collected from the encoder driver.
    @author             Damond Li
    @author             Chris Or
    @date               November 2, 2021
"""

import encoder
import utime
import math

class Task_Encoder:
    """ @brief                  Interface with quadrature encoders
        @details                This class implements a finite state machine 
                                and interfaces with the encoder through the 
                                encoder driver.
    """
    
    def __init__(self, period, state, UI_print, encodernum, speed):
        """ @brief              Constructs an encoder task
            @details            The encoder task is implemented as a finite
                                state machine.
            @param  period      The period, in seconds, between runs of 
                                the task.
            @param  state       A share. Share object describing the state of 
                                the finite state machine.
            @param  UI_print    A queue. Queue object containing the data to
                                be printed over the VCP
            @param  encodernum  A value identifying the encoder
        """
        ## @brief Create encoder object
        self.encoder = encoder.Encoder(encodernum)
        ## @brief A shares.Share object for the state of the finite state machine
        self.state = state
        ## @brief A value identifying the encoder
        self.encodernum = encodernum
        
        self.UI_print = UI_print
        
        self.speed = speed
        
        ## @brief Number of finite state machine runs
        self.runs = 0
        ## @brief Period at which to get data from encoder driver
        self.period = period
        ## @brief Reference object to determine elapsed time
        self.previous_ticks = utime.ticks_us()
        ## @brief Next instance to get data from encoder driver
        self.next_time = self.period
        ## @brief Boolean describing whether the 30-sec data collection is started
        self.boolean = False
        ## @brief Previous number of runs as a reference to calculate the delta value of runs
        self.prev_runs = self.runs
        
        self.timeoffset = float(0)
        
    def run(self):
        """ @brief              Runs one iteration of the finite state machine.
            @details            This function calls to the encoder driver at 
                                the specified period. 
        """
    
        self.encoder.update()
        
        self.current_ticks = utime.ticks_us()
        self.current_time = utime.ticks_diff(self.current_ticks, 
                                             self.previous_ticks) / 1_000_000
                                             
        if (self.current_time - self.next_time >= 0):
            if (self.state.read() == 0):
                ## @brief The number of runs in a period
                self.delta_runs = self.runs - self.prev_runs
                self.prev_runs = self.runs

                # Run State 0
                self.encoder.get_position()
                # Update next time variables
                self.next_time += self.period
                
        # State 1 Zeroes Position
        if (self.state.read() == 1):
            self.zero_pos()
            self.state.write(0)
            
        # State 2 Prints Position of Encoder
        elif (self.state.read() == 2):
            if (self.UI_print.num_in() == 0):
                self.UI_print.put(self.encoder.get_position())
                self.state.write(0)
        
        # State 3 Prints Delta of Encoder
        elif (self.state.read() == 3):
            if (self.UI_print.num_in() == 0):
                self.UI_print.put(self.encoder.get_delta())
                self.state.write(0)
             
        # State 4 Initiates the collection of encoder 1 data for 30 sec
        elif (self.state.read() == 4):
            # If data collection has not begun, boolean is false
            if not (self.boolean):
                self.startDataCollect()
            self.collectData()
            
        # State 6 Initiates the premature termination of the data collection
        elif (self.state.read() == 6):
            self.endDataCollect()
            
        self.get_omega()
            
        self.runs += 1

    def zero_pos(self):
        """ @brief              Zeroes the position of the encoder.
        """
        self.encoder.position = 0
            
    def startDataCollect(self):
        """ @brief              Starts data collection
            @details            This function initiates the collection of time 
                                and encoder position for 30s at intervals 
                                specified by the period in main. The
                                reference time is established.
        """
        self.boolean = True
        self.start_ticks = utime.ticks_us()
        
    def collectData(self):
        """ @brief              Period of data collection for 30 sec
            @details            This function collects data for the required 
                                30 sec by counting the time elapsed since the 
                                reference time. 
        """
        self.current_ticks = utime.ticks_us()
        self.current_time = utime.ticks_diff(self.current_ticks, 
                                             self.previous_ticks) / 1_000_000
        if (utime.ticks_diff(self.current_ticks, self.start_ticks) < 30_000_000):
            if (self.current_time - self.next_time >= 0):
                if (self.timeoffset == 0):
                    self.timeoffset = utime.ticks_diff(self.current_ticks, self.start_ticks)
                    
                self.UI_print.put([(utime.ticks_diff(
                    self.current_ticks, self.start_ticks)-self.timeoffset) / 1_000_000, 
                    self.encoder.get_position(), self.get_omega()])
                self.next_time += self.period
        else:
            self.endDataCollect()
        
    def endDataCollect(self):
        """ @brief              Ends data collection
            @details            This function terminates the collection of data 
                                by switching the boolean variable to false and
                                returning the state to zero.
        """
        self.timeoffset = 0
        if (self.boolean == True):
            self.boolean = False
            self.state.write(0)
        else:
            # Simply transition to state 0 if data collection did not start yet.
            self.state.write(0)
            
    def get_omega(self):
        """ @brief              Calculates the angular velocity of the motor
            @details            This function calculates the angular velocity in radians per second for the motor
                                using the number of runs per period and necessary unit conversions.
        """
        self.omega = self.encoder.get_delta() * (2*math.pi/4000) * (self.delta_runs/self.period)
        self.speed.write(self.omega)
        return self.omega
        

        